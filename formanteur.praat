#
# Analyseur de formants
# par Sylvain Didelot d'après un script de Sandra Schwab
# licence X11
#
name$ = "Formanteur"

# Configuration de base
beginPause(name$ + " - Paramètres")
    comment("La fenêtre suivante permet de sélectionner un dossier de sons.")
    comment("")
    comment("Créé dans le dossier des sons. ⚠ Si le fichier existe il sera remplacé !")
    sentence("Fichier pour les résultats", "résultats.xls")
    choice("Séparateur", 1)
        option("Tabulation")
        option("Virgule")
    comment("")
    comment("Si plusieurs phonèmes, les séparer par une virgule.")
    sentence("Phonème(s)", "")
    comment("")
    positive("left Formants à analyser (de...à)", 1)
    positive("right Formants à analyser (de...à)", 2)
select = endPause("Quitter", "Continuer", 2, 1)

if select == 1
    exitScript()
endif

dir$ = chooseDirectory$(name$ + " - Dossier contenant les sons.")
phonemes$ = "," + replace$(phonème$, " ", "", 0) + ","

if dir$ == "" or phonemes$ == ",,"
    writeInfoLine("Pas de dossier sélectionné ou pas de phonème.")
    exitScript()
endif

if windows
    dir$ = replace$(dir$, "\", "/", 0)
endif
if ! endsWith(dir$, "/")
    dir$ = dir$ + "/"
endif
s$ = séparateur$
if s$ == "tab"
    s$ = tab$
endif
save_file$ = dir$ + fichier_pour_les_résultats$
format$ = séparateur$
formants_min = left_Formants_à_analyser
formants_max = right_Formants_à_analyser

# préparer le tableau
h_data = do("Create Table with column names...", "data", 0, "Fichier Mot Sujet Liste Durée_mot Phonème Durée_phonème Durée_rel._phonème")
for i from formants_min to formants_max
    do("Append column...", "Moyenne_F" + string$(i))
    do("Append column...", "Début_F" + string$(i))
    do("Append column...", "Milieu_F" + string$(i))
    do("Append column...", "Fin_F" + string$(i))
endfor

# liste de tous les fichiers .wav dans chemin$
h_files = do("Create Strings as file list...", "Fichiers", dir$ + "*.wav")

# traiter chaque fichier
files_count = do("Get number of strings")
writeInfoLine("Traitement de ", files_count, " fichier(s)")
appendInfoLine()
for i to files_count
    selectObject(h_files)
    sound$ = do$("Get string...", i)

    # extraire info du nom du fichier
    file$ = left$(sound$, length(sound$) - 4)
    appendInfoLine("Traitement du fichier ", file$)
    x = length(file$) - 5
    subject$ = mid$(file$, x, 2)
    list$ = mid$(file$, x + 3, 3)
    grid$ = file$ + ".TextGrid"

    # lire et traiter le son
    h_sound = do("Read from file...", dir$ + sound$)

    #h_pitch = noprogress do("To Pitch...", 0, 75, 600)
    #selectObject(h_sound)
    h_formant = noprogress do("To Formant (burg)...", 0, 5, 5500, 0.025, 50)

    h_grid = do("Read from file...", dir$ + grid$)

    # traiter tous les mots du fichier (=intervals sur tire 3)
    words = do("Get number of intervals...", 3)
    for j to words
        word$ = do$("Get label of interval...", 3, j)
        appendInfoLine("    trouvé mot ", word$)

        word_start = do("Get starting point...", 3, j)
        word_end = do("Get end point...", 3, j)
        word_duration = (word_end - word_start)

        # traiter tous les phonèmes du mot (=intervals sur tire 1)
        word_interval_start = do("Get interval at time...", 1,
            ... word_start + 0.0001)
        word_interval_end = do("Get interval at time...", 1,
            ... word_end - 0.0001)
        if word_interval_start == 0 or word_interval_end == 0
            appendInfoLine("        aucun phonème trouvé.")
        else
            for k from word_interval_start to word_interval_end
                phoneme$ = do$("Get label of interval...", 1, k)
                appendInfoLine("        trouvé phonème ", phoneme$)

                # vérifier si le phonème nous intéresse
                if index(phonemes$, "," + phoneme$ + ",")
                    phoneme_start = do("Get starting point...", 1, k)
                    phoneme_end = do("Get end point...", 1, k)
                    phoneme_duration = (phoneme_end - phoneme_start)

                    selectObject(h_data)
                    do("Append row")
                    data_last = do("Get number of rows")
                    do("Set string value...", data_last, "Fichier", file$)
                    do("Set string value...", data_last, "Mot", word$)
                    do("Set string value...", data_last, "Sujet", subject$)
                    do("Set string value...", data_last, "Liste", list$)
                    do("Set string value...", data_last, "Durée_mot", fixed$(word_duration * 1000, 4))
                    do("Set string value...", data_last, "Phonème", phoneme$)
                    do("Set string value...", data_last, "Durée_phonème", fixed$(phoneme_duration * 1000, 4))
                    do("Set string value...", data_last, "Durée_rel._phonème", fixed$(phoneme_duration  / word_duration, 4))

                    # temps auquels les formants sont caculés
                    # 3 points: 1/3, 1/2, 2/3
                    time_1third = phoneme_start + phoneme_duration / 3
                    time_mid = (phoneme_start + phoneme_end) / 2
                    time_2third = phoneme_end - phoneme_duration / 3
                    # moyenne 60% milieu
                    time_start = phoneme_start + phoneme_duration / 5
                    time_end = phoneme_end - phoneme_duration / 5

                    for l from formants_min to formants_max
                        selectObject(h_formant)
                        formant_1third = do("Get value at time...", l,
                            ... time_1third, "Hertz", "Linear")
                        formant_mid = do("Get value at time...", l,
                            ... time_mid, "Hertz", "Linear")
                        formant_2third = do("Get value at time...", l,
                            ... time_2third, "Hertz", "Linear")
                        formant_mean = do("Get mean...", l, time_start,
                            ... time_end, "Hertz")

                        selectObject(h_data)
                        do("Set string value...", data_last, "Moyenne_F" + string$(l), fixed$(formant_mean, 0))
                        do("Set string value...", data_last, "Début_F" + string$(l), fixed$(formant_1third, 0))
                        do("Set string value...", data_last, "Milieu_F" + string$(l), fixed$(formant_mid, 0))
                        do("Set string value...", data_last, "Fin_F" + string$(l), fixed$(formant_2third, 0))
                    endfor
                    appendInfoLine("            traité")
                    selectObject(h_grid)
                endif
            endfor
        endif
    endfor

    # nettoyage
    removeObject(h_grid, h_sound, h_formant)
    #removeObject(h_pitch)
    appendInfoLine()
endfor

selectObject(h_data)
if format$ == "Virgule"
    do("Save as comma-separated file...", save_file$)
else
    do("Save as tab-separated file...", save_file$)
endif

removeObject(h_files, h_data)
appendInfoLine(files_count, " fichier(s) traité(s).")
