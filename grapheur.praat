#
# Représentation de l'espace vocalique
# par Sylvain Didelot d'après un script de Sandra Schwab
# licence X11
#
name$ = "Grapheur"

# translate colors
color_text$[0] = "Noir"
color_value$[0] = "Black"
color_text$[1] = "Rouge"
color_value$[1] = "Red"
color_text$[2] = "Vert"
color_value$[2] = "Green"
color_text$[3] = "Bleu"
color_value$[3] = "Blue"
color_text$[4] = "Jaune"
color_value$[4] = "Yellow"
color_text$[5] = "Cyan"
color_value$[5] = "Cyan"
color_text$[6] = "Magenta"
color_value$[6] = "Magenta"
color_text$[7] = "Maron"
color_value$[7] = "Maroon"
color_text$[8] = "Vert citron"
color_value$[8] = "Lime"
color_text$[9] = "Bleu marine"
color_value$[9] = "Navy"
color_text$[10] = "Bleu canard"
color_value$[10] = "Teal"
color_text$[11] = "Violet"
color_value$[11] = "Purple"
color_text$[12] = "Vert olive"
color_value$[12] = "Olive"
color_text$[13] = "Rose"
color_value$[13] = "Pink"
color_text$[14] = "Argent"
color_value$[14] = "Silver"
color_text$[15] = "Gris"
color_value$[15] = "Grey"
color_count = 15

# Configuration de base
beginPause(name$ + " - Paramètres")
    comment("Créé dans le dossier du script. ⚠ Si le fichier existe il sera remplacé !")
    sentence("Fichier pour le graphique", "espace_vocalique")
    comment("")
    positive("Formant axe X", 1)
    integer("left Echelle axe X (min, max)", 0)
    positive("right Echelle axe X (min, max)", 1000)
    comment("")
    positive("Formant axe Y", 2)
    integer("left Echelle axe Y (min, max)", 0)
    positive("right Echelle axe Y (min, max)", 1000)
    comment("")
    comment("⚠ La fenêtre 'Praat Picture' va ête réinitialisée !")
select = endPause("Quitter", "Continuer", 2, 1)

if select == 1
    exitScript()
endif
save_file$ = fichier_pour_le_graphique$

formant_x = formant_axe_X
formant_x_min = left_Echelle_axe_X
formant_x_max = right_Echelle_axe_X
formant_y = formant_axe_Y
formant_y_min = left_Echelle_axe_Y
formant_y_max = right_Echelle_axe_Y

# base du graphique
writeInfoLine("Préparation du graphique.")
do("Erase all")
do("Select outer viewport...", 0, 5, 0, 4)
do("Draw inner box")
do("Text top...", "yes", "Formant " + string$(formant_x))
do("Text right...", "yes", "Formant " + string$(formant_y))
do("Axes...", formant_x_max, formant_x_min, formant_y_max, formant_y_min)
do("Marks top...", 5, "yes", "yes", "yes")
do("Marks right...", 5, "yes", "yes", "yes")
group_label_count = 0
appendInfoLine()

# ajout d'un groupe
repeat
    beginPause(name$ + " - Ajout d'un groupe")
        comment("La fenêtre suivante permet de sélectionner un dossier pour ce groupe.")
        sentence("Libellé", "")
        word("Phonème", "")
        choice("Couleur", 1)
        for i from 0 to color_count
            option(color_text$[i])
        endfor
        comment("Sélectionner 'Quitter' lorsque tous les groupes ont été ajoutés.")
        comment("Le graphique sera sauvegardé comme '" + save_file$ + ".png'.")
    select = endPause("Quitter", "Ajouter", 2, 1)

    phoneme$ = ""
    dir$ = ""

    if select == 2
        phoneme$ = replace$(phonème$, " ", "", 0)
        dir$ = chooseDirectory$(name$ + " - Dossier contenant les sons.")
    endif

    if select == 2 and dir$ != "" and phoneme$ != ""
        for i from 0 to color_count
            if color_text$[i] == couleur$
                color$ = color_value$[i]
            endif
        endfor
        group_label$ = libellé$
        if windows
            dir$ = replace$(dir$, "\", "/", 0)
        endif
        if ! endsWith(dir$, "/")
            dir$ = dir$ + "/"
        endif

        # traiter chaque fichier
        h_data = do("Create Table with column names...", "data", 0, "phoneme formant_x formant_y")
        h_files = do("Create Strings as file list...", "files", dir$ + "*.wav")
        files_count = do("Get number of strings")
        appendInfoLine("Traitement de ", files_count, " fichier(s)")
        appendInfoLine()
        for i to files_count
            selectObject(h_files)
            sound$ = do$("Get string...", i)

            # extraire info du nom du fichier
            file$ = left$(sound$, length(sound$) - 4)
            appendInfoLine("Traitement du fichier ", file$)
            x = length(file$) - 5
            subject$ = mid$(file$, x, 2)
            list$ = mid$(file$, x + 3, 3)
            grid$ = file$ + ".TextGrid"

            # lire et traiter le son
            h_sound = do("Read from file...", dir$ + sound$)

            #h_pitch = noprogress do("To Pitch...", 0, 75, 600)
            #selectObject(h_sound)
            h_formant = noprogress do("To Formant (burg)...", 0, 5, 5500, 0.025, 50)

            h_grid = do("Read from file...", dir$ + grid$)

            # traiter tous les mots du fichier (=intervals sur tire 3)
            words = do("Get number of intervals...", 3)
            for j to words
                word$ = do$("Get label of interval...", 3, j)
                appendInfoLine("    trouvé mot ", word$)

                word_start = do("Get starting point...", 3, j)
                word_end = do("Get end point...", 3, j)
                word_duration = (word_end - word_start)

                # traiter tous les phonèmes du mot (=intervals sur tire 1)
                word_interval_start = do("Get interval at time...", 1,
                    ... word_start + 0.0001)
                word_interval_end = do("Get interval at time...", 1,
                    ... word_end - 0.0001)
                if word_interval_start == 0 or word_interval_end == 0
                    appendInfoLine("        aucun phonème trouvé.")
                else
                    for k from word_interval_start to word_interval_end
                        p$ = do$("Get label of interval...", 1, k)
                        appendInfoLine("        trouvé phonème ", p$)

                        # vérifier si le phonème nous intéresse
                        if phoneme$ == p$
                            phoneme_start = do("Get starting point...", 1, k)
                            phoneme_end = do("Get end point...", 1, k)
                            phoneme_duration = (phoneme_end - phoneme_start)

                            # temps auquels les formants sont caculés
                            # moyenne 60% milieu
                            time_start = phoneme_start + phoneme_duration / 5
                            time_end = phoneme_end - phoneme_duration / 5

                            selectObject(h_formant)
                            formant_x_mean = do("Get mean...", formant_x, time_start,
                            ... time_end, "Hertz")
                            formant_y_mean = do("Get mean...", formant_y, time_start,
                            ... time_end, "Hertz")

                            selectObject(h_data)
                            do("Append row")
                            data_last = do("Get number of rows")
                            do("Set string value...", data_last, "phoneme", p$)
                            do("Set numeric value...", data_last, "formant_x", formant_x_mean)
                            do("Set numeric value...", data_last, "formant_y", formant_y_mean)

                            selectObject(h_grid)

                            appendInfoLine("            traité")
                        endif
                    endfor
                endif
            endfor

            # nettoyage
            removeObject(h_grid, h_sound, h_formant)
            #removeObject(h_pitch)
            appendInfoLine()
        endfor

        # ajouter sur le graph
        appendInfoLine("Ajout au graphique.")
        selectObject(h_data)
        do(color$)
        do("Select outer viewport...", 0, 5, 0, 4)
        do("Scatter plot...", "formant_x", formant_x_max, formant_x_min, "formant_y", formant_y_max, formant_y_min, "phoneme", 10, "no")
        if group_label$ != ""
            do("Select outer viewport...", 1, 5, 4 + group_label_count * 0.2, 4.2 + group_label_count * 0.2)
            do("Viewport text...", "Left", "Half", 0, group_label$)
            group_label_count = group_label_count + 1
        endif
        removeObject(h_data, h_files)
        appendInfoLine()
    endif
until select == 1

appendInfoLine("Sauvegarde du graphique.")
do("Select outer viewport...", 0, 5, 0, 4 + group_label_count * 0.2)
do("Save as 600-dpi PNG file...", save_file$ + ".png")
do("Save as praat picture file...", save_file$ + ".prapic")
