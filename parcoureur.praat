#
# Parcoureur de dossier
# par Sylvain Didelot
# licence X11
#
name$ = "Parcoureur"

# sélectionner le dossier
dir$ = chooseDirectory$(name$ +" - Dossier à traiter")
if dir$ == ""
    writeInfoLine("Aucun dossier selectionné.")
    exitScript()
endif
if windows
    dir$ = replace$(dir$, "\", "/", 0)
endif
if ! endsWith(dir$, "/")
    dir$ = dir$ + "/"
endif

# restaurer le fichier en cours si souhaité
restore$ = dir$ + name$ + ".txt"
i = 1
if fileReadable(restore$)
    beginPause(name$)
        comment("Travail précédent détecté, reprendre?")
    select = endPause("Non", "Oui", 2, 1)

    if select == 2
        i = readFile(restore$)
    else
        deleteFile(restore$)
    endif
endif

# lister les fichiers dans le dossier
h_files = do("Create Strings as file list...", "Fichiers", dir$ + "*.wav")
files_count = do("Get number of strings")

while i <= files_count
    selectObject(h_files)
    # ouvrir les fichiers (créer le textgrid si nécessaire)
    sound$ = do$("Get string...", i)
    h_sound = do("Read from file...", dir$ + sound$)
    grid$ = left$(sound$, length(sound$) - 4) + ".TextGrid"
    if fileReadable(dir$ + grid$)
        h_grid = do("Read from file...", dir$ + grid$)
    else
        duration = do("Get total duration")
        h_grid = do("Create TextGrid...", 0, duration,
        ... "phones syll words phono ortho", "")
    endif

    # lancer l'édition
    selectObject(h_grid, h_sound)
    do("Edit")

    # attendre les instructions de l'utilisateur
    beginPause(name$)
        comment("Fichier " + string$(i) + " sur " + string$(files_count))
        comment("Le fichier en cours sera sauvé, sauf si rechargé (↻).")
    select = endPause("Marre!", "←", "↻", "→", 4, 1)

    # sauver et nettoyer
    if select != 3
        selectObject(h_grid)
        do("Save as text file...", dir$ + grid$)
    endif
    removeObject(h_grid, h_sound)

    # agire en fonction du choix
    if select == 1
        removeObject(h_files)
        writeFile(restore$, i)
        exitScript()
    elif select == 2 and i > 1
        i = i - 1
    elif select == 4 and i <= files_count
        i = i + 1
    endif
endwhile

deleteFile(restore$)
removeObject(h_files)
writeInfoLine(files_count, " fichier(s) traité(s). Ça mérite un sugus!")
